TARGET = TinyXML
include($$PWD/../../../commonplatform.pri)
CONFIG += staticlib

include(TinyXML.pri)

load(qt_helper_lib)

clang {
#Ignore selected warnings from TinyXML lib (as it's a 3rd party lib)
QMAKE_CXXFLAGS_WARN_ON = -Wall \
    -Wno-logical-op-parentheses
}
