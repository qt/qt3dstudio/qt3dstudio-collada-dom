TARGET = ColladaDOM
include($$PWD/../../commonplatform.pri)
CONFIG += staticlib
DEFINES += HAVE_CONFIG_H DOM_INCLUDE_TINYXML PCRE_STATIC NO_BOOST \
    NO_ZAE COLLADA_DOM_SUPPORT141 DOM_DYNAMIC DOM_EXPORT _LIBCPP_ENABLE_CXX17_REMOVED_AUTO_PTR
win32 {
    QMAKE_LFLAGS += /NODEFAULTLIB:tinyxml.lib
    DEFINES += "_HAS_AUTO_PTR_ETC=1"
    mingw {
        DEFINES += WIN32
    }
}

#Ignore selected warnings from Collada lib (as it's a 3rd party lib)
clang {
QMAKE_CXXFLAGS_WARN_ON = -Wall \
    -Wno-unused-parameter \
    -Wno-header-guard \
    -Wno-null-conversion \
    -Wno-overloaded-virtual \
    -Wno-unused-function \
    -Wno-old-style-cast \
    -Wno-logical-op-parentheses
}

mingw {
LIBS += \
    -lpcre$$qtPlatformTargetSuffix() \
    -lTinyXML$$qtPlatformTargetSuffix()
}

INCLUDEPATH += \
    ../pcre/8.31 \
    TinyXML/2.5.3 \
    2.4.0/dom/include

include(ColladaDOM.pri)

load(qt_helper_lib)

CONFIG += exceptions
